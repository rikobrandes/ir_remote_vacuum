// This #include statement was automatically added by the Particle IDE.
#include "IRremote.h"

//const int buttonCodes[]    = {0x818100FF, 0x8181807F, 0x818140BF, 0x8181C03F, 0x818120DF, 0x818110EF, 0x8181906F, 0x818150AF, 0x8181D02F, 0x818130CF, 0x8181B04F, 0x8181708F};
//const char * buttonNames[] = {"power", "recharging", "auto", "spot", "max", "startStop", "manual", "edge", "timeDaily", "clock", "extraLeft", "extraRight"};
//const int arraySize = sizeof(buttonNames)/sizeof(buttonCodes[0]);


/* Example program for from IRLib – an Arduino library for infrared encoding and decoding
   Version 1.3   January 2014
   Copyright 2014 by Chris Young http://cyborg5.com
   Based on original example sketch for IRremote library
   Version 0.11 September, 2009
   Copyright 2009 Ken Shirriff
   http://www.righto.com/
*/
#include <IRremote.h>

#define POWER 0x818100FF
#define RECHARGING 0x8181807F

#define AUTO 0x818140BF
#define SPOT 0x8181C03F
#define MAX 0x818120DF

//#define CURSORUP
//#define CURSORRIGHT
//#define CURSURLEFT
#define LEDIR D0

#define STARTSTOP 0x818120DF
#define MANUAL 0x818110EF
#define EDGE 0x8181906F
#define TIMEDAILY 0x818150AF
#define CLOCK 0x8181D02F

#define EXTRALEFT 0x8181B04F
#define EXTRARIGHT 0x8181708F

const boolean debug = false;
const char * feedbackStrings[] = {"power", "recharging", "auto", "spot", "max", "startStop", "manual", "edge", "timeDaily", "clock", "extraLeft", "extraRight"};
const int buttonCodes[]  = {0x818100FF, 0x8181807F, 0x818140BF, 0x8181C03F, 0x818120DF, 0x818110EF, 0x8181906F, 0x818150AF, 0x8181D02F, 0x818130CF, 0x8181B04F, 0x8181708F};

IRsend MySender(LEDIR);

void setup()
{
  //Serial.begin(9600);
}

void loop() {

  //if (debug) test();

  test();
  delay(5000);

}


// TEST FUNCTION
void test() {
  //send a code  every time the correspoding character is received from the serial port
  char command = '1';

  if (command == '1') {
    MySender.sendNEC2(POWER, 32);
    //Serial.println("Power command sent");
  }

  if (command == '2') {
    MySender.sendNEC2( RECHARGING, 32);
    //Serial.println("Recharging command sent");
  }

  if (command == '3') {
    MySender.sendNEC2( POWER, 32);
    //Serial.println("Power command sent");

    delay(3000);
    MySender.sendNEC2( MAX, 32);
    //Serial.print("Max command sent");
  }

  delay(3000);
}
